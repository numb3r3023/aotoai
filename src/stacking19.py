import pandas as pd
import numpy as np
import gc
import feather

import lightgbm as lgb
import xgboost as xgb

from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold

from sklearn.metrics import log_loss
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import scale
from rgf.sklearn import RGFClassifier

from utils import *

np.random.seed(SEED)

import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.wrappers.scikit_learn import KerasClassifier


"""
N-diverse models are trained on same dataset.
"""

EXPERIMENT_NAME  = 'STACKING19'


def load_datasets(experiment_names, model_names):
	train_datasets = []
	test_datasets  = []

	for i in range(len(experiment_names)):
		experiment_name = experiment_names[i]
		model_name      = model_names[i]

		train_datasets.append(feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name))))
		test_datasets.append(feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_test.feather'%(experiment_name, model_name))))

	return train_datasets, test_datasets

def model_configuration():
	# create model
	model = Sequential(name='mlp')

	model.add(Dense(units=50, activation='relu', input_dim=49))
	model.add(Dropout(rate=.1))
	model.add(Dense(units=20, activation='relu'))
	model.add(Dropout(rate=.1))
	model.add(Dense(units=3, activation='softmax'))

	model.compile("adam", "categorical_crossentropy", metrics=['accuracy'])

	return model


def train_diverse_models(train_datasets, test_datasets, experiment_names, model_names, models):
	N_MODELS = len(models)

	train_meta = np.zeros(shape=(len(train_datasets[0]), 3 * N_MODELS))
	kf       = KFold(n_splits=3, shuffle=False, random_state=SEED)

	for i in range(len(train_datasets)):
		print('Training dataset ', experiment_names[i])
		print('Model ', model_names[i])
		print('*' * 100)
		print()

		features = train_datasets[i].columns.drop(['outcome', 'id', 'train', 'submission_id'])


		X = train_datasets[i].loc[:, features]
		y = train_datasets[i].loc[:, 'outcome']

		for fold_index, (itr, ite) in enumerate(kf.split(X)):
	
			print('Fold index: {}'.format(fold_index))

			Xtr = X.iloc[itr]
			ytr = y.iloc[itr]

			Xte = X.iloc[ite]
			yte = y.iloc[ite]

			if models[i] == 'lgb':
				params = joblib.load('../models/%s_%s_params_full.pkl'%(experiment_names[i], model_names[i]))

				ltrain = lgb.Dataset(Xtr, ytr)
				num_boost_round = params['num_boost_round']
				del params['num_boost_round']

				model  = lgb.train(params, ltrain, num_boost_round)
				train_meta[ite, 3*i:3*i+3] = model.predict(Xte)

			elif models[i] == 'xgb':
				params = joblib.load('../models/%s_%s_params_full.pkl'%(experiment_names[i], model_names[i]))

				dtrain = xgb.DMatrix(Xtr, ytr)
				deval  = xgb.DMatrix(Xte)

				num_boost_round = params['num_boost_round']
				del params['num_boost_round']

				model  = xgb.train(params, dtrain, num_boost_round)
				train_meta[ite, 3*i:3*i+3] = model.predict(deval)
			elif models[i] == 'nn':
				estimator = KerasClassifier(build_fn=model_configuration, epochs=10, verbose=0)
				
				Xtr = scale(Xtr)
				ytr = pd.get_dummies(ytr).values
				Xte = scale(Xte)

				estimator.fit(Xtr, ytr)

				train_meta[ite, 3*i:3*i+3] = estimator.predict_proba(Xte)
			else:
				params = joblib.load('../models/%s_%s_params.pkl'%(experiment_names[i], model_names[i]))

				model = RGFClassifier(**params)
				
				Xtr = scale(Xtr)
				Xte = scale(Xte)

				model.fit(Xtr, ytr)

				train_meta[ite, 3*i:3*i+3] = model.predict_proba(Xte)



	return train_meta

def calculate_test_meta(train_datasets, test_datasets, experiment_names, model_names, models):
	N_MODELS  = len(models)
	test_meta = np.zeros(shape=(len(test_datasets[0]), 3 * N_MODELS))
	sub       = load_sub()  

	for i in range(len(experiment_names)):
		features  = train_datasets[i].columns.drop(['outcome', 'id', 'train', 'submission_id'])

		print()
		print('Dataset: ', experiment_names[i])
		print('Model name: ', model_names[i])
		print('*' * 100)
		print()

		X = train_datasets[i].loc[:, features]
		y = train_datasets[i].loc[:, 'outcome']

		test_sub  = sub.merge(test_datasets[i], on='submission_id', how='left')
		X_test    = test_sub.loc[:, features]

		if models[i] == 'lgb':
			params = joblib.load('../models/%s_%s_params_full.pkl'%(experiment_names[i], model_names[i]))

			ltrain = lgb.Dataset(X, y)
			num_boost_round = params['num_boost_round']
			del params['num_boost_round']

			model  = lgb.train(params, ltrain, num_boost_round)
			test_meta[:, 3*i:3*i+3] = model.predict(X_test)

		elif models[i] == 'xgb':
			params = joblib.load('../models/%s_%s_params_full.pkl'%(experiment_names[i], model_names[i]))

			dtrain = xgb.DMatrix(X, y)
			deval  = xgb.DMatrix(X_test)

			num_boost_round = params['num_boost_round']
			del params['num_boost_round']

			model  = xgb.train(params, dtrain, num_boost_round)
			test_meta[:, 3*i:3*i+3] = model.predict(deval)
		elif models[i] == 'nn':
			estimator = KerasClassifier(build_fn=model_configuration, epochs=10, verbose=0)
			
			X = scale(X)
			y = pd.get_dummies(y).values
			X_test = scale(X_test)

			estimator.fit(X, y)

			test_meta[:, 3*i:3*i+3] = estimator.predict_proba(X_test)

		else:
			params = joblib.load('../models/%s_%s_params.pkl'%(experiment_names[i], model_names[i]))

			model = RGFClassifier(**params)
			
			X      = scale(X)
			X_test = scale(X_test)

			model.fit(X, y)

			test_meta[:, 3*i:3*i+3] = model.predict_proba(X_test)

	return test_meta

def cross_validate_meta_model(train_meta, y):
	dtrain_meta = xgb.DMatrix(train_meta, y)

	params = {
			'eta': .1,
			'objective': 'multi:softprob',
			'max_depth': 2,
			'eval_metric': 'mlogloss',
			'num_class': 3,
			'seed': SEED,
			'verbose': 1,
			'nthread': 4,
			'silent': 1
		}

	num_boost_round = 500
	early_stopping_rounds = 40

	cv_summary = xgb.cv(params, 
		dtrain_meta, 
		num_boost_round,
		nfold=7,
		stratified=True,
		early_stopping_rounds=early_stopping_rounds,
		verbose_eval=20,
		seed=SEED
	)

	best_iteration = len(cv_summary)
	params['num_boost_round'] = best_iteration
	return cv_summary.iloc[-1]['test-mlogloss-mean'], cv_summary.iloc[-1]['test-mlogloss-std'], params

def train_meta_model(train_meta, y, test_meta, params):
	best_iteration = params['num_boost_round']
	del params['num_boost_round']

	dtrain_meta  = xgb.DMatrix(train_meta, y)
	dtest_meta   = xgb.DMatrix(test_meta)

	num_boost_round = int(best_iteration * 1.1)
	params['eta'] = params['eta'] / 1.1

	print()
	print('params ', params)
	model = xgb.train(params, dtrain_meta, num_boost_round)
	final_preds = model.predict(dtest_meta)

	sub = load_sub()
	sub.loc[:, ['UE', 'FE', 'W']] = final_preds

	model_name      = 'lgb_nn_xgb_rgf'

	filename = create_filename(model_name, cv_score='0.3129+0.0159')
	sub.to_csv(os.path.join(basepath, 'submissions/%s_%s_%s.csv'%(EXPERIMENT_NAME, model_name, filename)), index=False)



def main(cv):
	experiment_names = [
						# 'EXPERIMENT34', 
						# 'EXPERIMENT40', 
						'EXPERIMENT40', 
						# 'EXPERIMENT35', 
						# 'EXPERIMENT36',
						'EXPERIMENT21',
						'EXPERIMENT49',
						'EXPERIMENT51',
						'EXPERIMENT59'

						]

	model_names      = [
						# 'LGB%s'%(SEED), 
						# 'LGB%s'%(SEED + 100), 
						'LGB%s'%(SEED), 
						# 'LGB%s'%(SEED),
						# 'LGB%s'%(SEED),
						'NN%s'%(SEED),
						'XGB%s'%(SEED),
						'RGF%s'%(SEED),
						'LGB%s'%(SEED)

					   ]

	models           = [
						# 'lgb', 
						# 'lgb', 
						'lgb', 
						# 'lgb', 
						# 'lgb',
						'nn',
						'xgb',
						'rgf',
						'lgb'
					   ]

	if not os.path.exists(os.path.join(basepath, 'data/processed/%s_train_meta.pkl'%(EXPERIMENT_NAME))):
		train_datasets, test_datasets = load_datasets(experiment_names, model_names)
		train_meta = train_diverse_models(train_datasets, test_datasets, experiment_names, model_names, models)
		test_meta  = calculate_test_meta(train_datasets, test_datasets, experiment_names, model_names, models)

		print('train meta shape : ',  train_meta.shape)
		print('test meta shape : ', test_meta.shape)

		y = train_datasets[0].loc[:, 'outcome']

		joblib.dump(train_meta, os.path.join(basepath, 'data/processed/%s_train_meta.pkl'%(EXPERIMENT_NAME)))
		joblib.dump(test_meta, os.path.join(basepath, 'data/processed/%s_test_meta.pkl'%(EXPERIMENT_NAME)))
		joblib.dump(y, os.path.join(basepath, 'data/processed/%s_target.pkl'%(EXPERIMENT_NAME)))

	else:
		print()
		print('Loading from disk ...')
		train_meta = joblib.load(os.path.join(basepath, 'data/processed/%s_train_meta.pkl'%(EXPERIMENT_NAME)))
		test_meta  = joblib.load(os.path.join(basepath, 'data/processed/%s_test_meta.pkl'%(EXPERIMENT_NAME)))
		y          = joblib.load(os.path.join(basepath, 'data/processed/%s_target.pkl'%(EXPERIMENT_NAME)))

	if cv:
		cv_mean, cv_std, params = cross_validate_meta_model(train_meta, y)
		print('params ', params)
		print()
		print('mean: {}, std: {}'.format(cv_mean, cv_std))
		joblib.dump(params, os.path.join(basepath, 'models/%s_params_full.pkl'%(EXPERIMENT_NAME)))
	else:
		params = joblib.load(os.path.join(basepath, 'models/%s_params_full.pkl'%(EXPERIMENT_NAME)))
		train_meta_model(train_meta, y, test_meta, params)


if __name__ == '__main__':
	main(cv=True)