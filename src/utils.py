import pandas as pd
import numpy as np
import os
import datetime

SEED = 1982

from sklearn.model_selection import StratifiedKFold

basepath = os.path.expanduser('~/Desktop/src/ml/ao_to_ai/')


"""
Create a function to prepare training and test data for us.

If split = True then we return train_sub and test_sub folds instead of train and test
"""

def load_train():
    men_train   = pd.read_csv(os.path.join(basepath, 'data/raw/mens_train_file.csv'))
    women_train = pd.read_csv(os.path.join(basepath, 'data/raw/womens_train_file.csv'))
    
    train     = pd.concat((men_train, women_train))
    
    # UE - 0
    # FE - 1
    # W  - 2
    target_mapping = {
        'UE' : 0,
        'FE' : 1,
        'W'  : 2
    }
    
    train.loc[:, 'outcome'] = train.outcome.map(target_mapping)
    
    return train

def load_test():
    men_test    = pd.read_csv(os.path.join(basepath, 'data/raw/mens_test_file.csv'))
    women_test  = pd.read_csv(os.path.join(basepath, 'data/raw/womens_test_file.csv'))
    
    test  = pd.concat((men_test, women_test))
    
    return test    

def get_data(split=False):
    if split:
        # create folds and return folds
        train = load_train()

        skf = StratifiedKFold(n_splits=3, random_state=SEED, shuffle=False)
        itr, ite = next(skf.split(train, train['outcome']))
        
        train = train.iloc[itr]
        test  = train.iloc[ite]
        
    else:
        train = load_train()
        test  = load_test()
        
    return train, test

"""
Function to load sample submission file
"""

def load_sub():
    sub = pd.read_csv(os.path.join(basepath, 'data/raw/AUS_SubmissionFormat.csv'))
    
    return sub


"""
Label Encode categorical features
"""

def label_encode_categorical(traintest, columns):

	for col in columns:
		traintest.loc[:, col] = pd.factorize(traintest.loc[:, col])[0]

	return traintest


"""
One hot encode categorical features
"""

def one_hot_encode(traintest, columns):
	return pd.get_dummies(traintest.loc[:, columns], 
						  drop_first=True)


"""
Creates filename out of model_name and cv_score,
so if cv_score == 0, it means we didn't do cross-validation for that 
model.
"""
def create_filename(model_name, cv_score=0.):
	name = "%s_%s_%s" % (datetime.datetime.now().strftime('%Y%m%d_%H%M'), model_name, str(cv_score).replace('.', '_'))
	return name





