import pandas as pd
import numpy as np
import gc
import feather

import lightgbm as lgb
import xgboost as xgb

from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold

from sklearn.metrics import log_loss
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import scale

from sklearn.neighbors import KNeighborsClassifier

from utils import *

np.random.seed(SEED)


"""
N-diverse models are trained on same dataset.
"""

EXPERIMENT_NAME  = 'STACKING2ndLevel1'


def load_datasets(experiment_names):
	train_datasets = []
	test_datasets  = []

	for i in range(len(experiment_names)):
		experiment_name = experiment_names[i]
		
		train_datasets.append(joblib.load(os.path.join(basepath, 'data/processed/%s_train_meta.pkl'%(experiment_name))))
		test_datasets.append(joblib.load(os.path.join(basepath, 'data/processed/%s_test_meta.pkl'%(experiment_name))))

	return train_datasets, test_datasets

def train_diverse_models(train_datasets, test_datasets, experiment_names, models):
	N_MODELS = len(models)

	train_meta = np.zeros(shape=(len(train_datasets[0]), 3 * N_MODELS))
	kf         = KFold(n_splits=3, shuffle=False, random_state=SEED)

	for i in range(len(train_datasets)):
		print('Training dataset ', experiment_names[i])
		print('*' * 100)
		print()

		X = train_datasets[i]
		y = joblib.load(os.path.join(basepath, 'data/processed/%s_target.pkl'%(experiment_names[i]))).values

		for fold_index, (itr, ite) in enumerate(kf.split(X)):
	
			print('Fold index: {}'.format(fold_index))

			Xtr = X[itr]
			ytr = y[itr]

			Xte = X[ite]
			yte = y[ite]

			if models[i] == 'knn':
				model = KNeighborsClassifier(n_neighbors=100)
				model.fit(Xtr, ytr)

				train_meta[ite, 3*i:3*i+3] = model.predict_proba(Xte)
			else:
				model = LogisticRegression(random_state=SEED)
				model.fit(Xtr, ytr)

				train_meta[ite, 3*i:3*i+3] = model.predict_proba(Xte)

	return train_meta

def calculate_test_meta(train_datasets, test_datasets, experiment_names, models):
	N_MODELS  = len(models)
	test_meta = np.zeros(shape=(len(test_datasets[0]), 3 * N_MODELS))
	sub       = load_sub()  

	for i in range(len(experiment_names)):
		print()
		print('Dataset: ', experiment_names[i])
		print('*' * 100)
		print()

		X = train_datasets[i]
		y = joblib.load(os.path.join(basepath, 'data/processed/%s_target.pkl'%(experiment_names[i]))).values
		
		tdf       = feather.read_dataframe(os.path.join(basepath, 'data/processed/EXPERIMENT36_LGB1982_test.feather'))
		test_df   = pd.DataFrame(test_datasets[i], 
								columns=['col_%d'%i for i in range(test_datasets[i].shape[1])],
 								index=tdf.index.values
								)

		test_df   = pd.concat((test_df, tdf[['submission_id']]), axis='columns')

		test_sub  = sub.merge(test_df, on='submission_id', how='left')
		X_test    = test_sub.loc[:, test_sub.columns.drop(['submission_id', 'train', 'UE', 'FE', 'W'])]

		if models[i] == 'knn':
			model = KNeighborsClassifier(n_neighbors=10)
			model.fit(X, y)

			test_meta[:, 3*i:3*i+3] = model.predict_proba(X_test)
		else:
			model = LogisticRegression()
			model.fit(X, y)

			test_meta[:, 3*i:3*i+3] = model.predict_proba(X_test)

	return test_meta

def cross_validate_meta_model(train_meta, y):
	params = {
			'C': 1.0,
			'random_state': SEED
		}

	log = LogisticRegression(**params)

	cv_scores = cross_val_score(log, train_meta, y, cv=7, scoring='neg_log_loss', n_jobs=-1)

	return -np.mean(cv_scores), np.std(cv_scores), params

def train_meta_model(train_meta, y, test_meta, params):
	model = LogisticRegression(**params)
	final_preds = model.predict_proba(test_meta)

	sub = load_sub()
	sub.loc[:, ['UE', 'FE', 'W']] = final_preds

	model_name      = 'lgb_nn_xgb_rgf'

	filename = create_filename(model_name, cv_score='0.3129+0.0159')
	sub.to_csv(os.path.join(basepath, 'submissions/%s_%s_%s.csv'%(EXPERIMENT_NAME, model_name, filename)), index=False)



def main(cv):
	experiment_names = [
						'STACKING14', 
						'STACKING19'
						]

	models           = [
						'knn', 
						'log'
					   ]

	if not os.path.exists(os.path.join(basepath, 'data/processed/%s_train_meta.pkl'%(EXPERIMENT_NAME))):
		train_datasets, test_datasets = load_datasets(experiment_names)
		train_meta = train_diverse_models(train_datasets, test_datasets, experiment_names, models)
		test_meta  = calculate_test_meta(train_datasets, test_datasets, experiment_names, models)

		print('train meta shape : ',  train_meta.shape)
		print('test meta shape : ', test_meta.shape)

		y = joblib.load(os.path.join(basepath, 'data/processed/STACKING14_target.pkl')).values
		
		joblib.dump(train_meta, os.path.join(basepath, 'data/processed/%s_train_meta.pkl'%(EXPERIMENT_NAME)))
		joblib.dump(test_meta, os.path.join(basepath, 'data/processed/%s_test_meta.pkl'%(EXPERIMENT_NAME)))
		joblib.dump(y, os.path.join(basepath, 'data/processed/%s_target.pkl'%(EXPERIMENT_NAME)))

	else:
		print()
		print('Loading from disk ...')
		train_meta = joblib.load(os.path.join(basepath, 'data/processed/%s_train_meta.pkl'%(EXPERIMENT_NAME)))
		test_meta  = joblib.load(os.path.join(basepath, 'data/processed/%s_test_meta.pkl'%(EXPERIMENT_NAME)))
		y          = joblib.load(os.path.join(basepath, 'data/processed/%s_target.pkl'%(EXPERIMENT_NAME)))

	if cv:
		cv_mean, cv_std, params = cross_validate_meta_model(train_meta, y)
		print('params ', params)
		print()
		print('mean: {}, std: {}'.format(cv_mean, cv_std))
		joblib.dump(params, os.path.join(basepath, 'models/%s_params_full.pkl'%(EXPERIMENT_NAME)))
	else:
		params = joblib.load(os.path.join(basepath, 'models/%s_params_full.pkl'%(EXPERIMENT_NAME)))
		train_meta_model(train_meta, y, test_meta, params)


if __name__ == '__main__':
	main(cv=True)