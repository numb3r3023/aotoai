import pandas as pd
import feather
import os

import numpy as np
import xgboost as xgb
import xgbfir

basepath = os.path.expanduser('~/Desktop/src/ml/ao_to_ai/')

SEED = 1928
np.random.seed(SEED)

def main():
	train = feather.read_dataframe(os.path.join(basepath, 'data/processed/EXPERIMENT1_XGB1982_train.feather'))

	# train xgboost model
	features = train.columns.drop(['outcome', 'id', 'train', 'submission_id'])

	X = train.loc[:, features]
	y = train['outcome']

	dtrain = xgb.DMatrix(X, y, feature_names=features.tolist())

	# parameters of the model
	params = {
			'eta': .1,
			'objective': 'multi:softprob',
			'max_depth': 5,
			'eval_metric': 'mlogloss',
			'num_class': 3,
			'seed': SEED,
			'verbose': 1,
			'nthread': 4,
			'silent': 1
	}

	num_boost_round = 100
	xgb_model = xgb.train(params, dtrain, num_boost_round)

	xgbfir.saveXgbFI(xgb_model, 
					feature_names=features.tolist(), 
					OutputXlsxFile=os.path.join(basepath, 'data/interim/fir.xlsx'))


if __name__ == '__main__':
	main()