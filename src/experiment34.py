AIM = """
LightGBM model on all dataset used in experiment 33 + knn based features ( OOF )

"""

import pandas as pd
import numpy as np
import gc
import feather

from utils import *
from knn_features import *

from sklearn.externals import joblib
from sklearn.metrics import log_loss
from sklearn.model_selection import cross_val_predict

import lightgbm as lgb


experiment_name = 'EXPERIMENT34'
model_name      = 'LGB%s'%(SEED)

np.random.seed(SEED)

def create_features(traintest, split):

	if not split:
		# create a feature for helping to merge with submission file
		traintest.loc[:, 'submission_id'] = traintest.loc[:, 'id'].astype(np.str) + '_' + traintest.loc[:, 'gender']
	

	# flag if net clearance is negative or not
	traintest.loc[:, 'below_net'] = (traintest['net.clearance'] < 0).astype(np.uint8)
	
	# interaction between outside baseline and outside sideline net clearance greater than zero or not.
	# since this is a categorical variable we would have to one-hot encode this.
	traintest.loc[:, 'out_of_bounds_or_below_net'] = (traintest['outside.sideline']).astype(np.str) + '_' +\
													 (traintest['outside.baseline']).astype(np.str) + '_' +\
													 (traintest['net.clearance'] < 0). astype(np.str)
	

	# feature to capture ratio of change in speed from previous shot
	# to this shot.
	traintest.loc[:, 'speed_ratio'] = (traintest['speed'] / traintest['previous.speed'])

	# feature to capture ratio of current depth from baseline to previous shot
	traintest.loc[:, 'depth_ratio'] = (traintest['depth'] / traintest['previous.depth'])

	# feature to capture good angeled shot
	traintest.loc[:, 'angeled_shot'] = (traintest['distance.from.sideline'] / traintest['previous.distance.from.sideline']) * traintest['player.distance.travelled']

	# feature to capture change in player's depth from penultimate shot to point-ending shot
	traintest.loc[:, 'player_depth_ratio'] = (traintest['player.impact.depth'] / traintest['player.depth'])
	traintest.loc[:, 'player_depth_diff']  = (traintest['player.impact.depth'] - traintest['player.depth'])

	# feature to capture player's depths
	traintest.loc[:, 'player_depth_comp'] = (traintest['player.impact.depth'] / traintest['opponent.depth'])

	# feature to capture speed of penultimate shot's speed
	traintest.loc[:, 'prev_shot_speed']   = (traintest['player.distance.travelled'] / traintest['previous.time.to.net'])

	# feature to capture relationship between ball bounce distance from net and player's distance from net.
	traintest.loc[:, 'ball_player_depth_relation'] = (traintest['depth'] * traintest['player.impact.depth'])

	# feature to compare ball distance from sideline and distance travelled by player across last two shots
	# if lateral distance of ball from sideline is low and player distance is huge then kudos to opponent
	# for making the player returning the shot off-balance.

	traintest.loc[:, 'ball_player_dist_sideline'] = (traintest['previous.distance.from.sideline'] - traintest['distance.from.sideline']) / (traintest['player.distance.travelled'])

	# feature to capture difference between ball speeds and distance travelled by player across last two
	# shots of the point.

	traintest.loc[:, 'ball_speeds_player_dist'] = (traintest['speed'] - traintest['previous.speed']) / (traintest['player.distance.travelled'])
	traintest.loc[:, 'deceleration_rate'] = (traintest['speed'] - traintest['previous.speed']) / traintest['previous.time.to.net']
	traintest.loc[:, 'lob'] = (traintest['net.clearance'] * (traintest['depth'] - traintest['opponent.depth']))

	traintest.loc[:, 'hyp_with_euclidean'] = ((traintest['distance.from.sideline']) ** 2 +\
									 (traintest['depth']) ** 2) -\
									 traintest['player.distance.travelled']

	traintest.loc[:, 'slice_shot'] = (traintest['net.clearance']) * (traintest['speed'] / traintest['previous.speed'])

	traintest.loc[:, 'tough_shot_return'] = (traintest['previous.distance.from.sideline'] / traintest['speed']) - traintest['previous.time.to.net']

	traintest.loc[:, 'easy_shot'] = (np.abs(traintest['depth'] - 6) + traintest['player.distance.travelled'] + traintest['player.impact.distance.from.center']) * (traintest['net.clearance'])

	traintest.loc[:, 'shot_beside_baseline'] = traintest['net.clearance'] * traintest['previous.distance.from.sideline']

	# One hot encode following categorical variables
	#
	# 1. Serve
	# 2. hitpoint
	# 3. outside.sideline
	# 4. outside.baseline
	# 5. same.side
	# 6. previous.hitpoint
	# 7. server.is.impact.player
	# 8. gender

	features_to_ohe = ['serve', 
					   'hitpoint',
					   'outside.sideline',
					   'outside.baseline',
					   'same.side',
					   'previous.hitpoint',
					   'server.is.impact.player',
					   'gender',
					   'out_of_bounds_or_below_net'
					  ]

	one_hot_encoded_features = one_hot_encode(traintest.loc[:, features_to_ohe].astype(np.str), features_to_ohe)
	ohe_feature_names        = one_hot_encoded_features.columns.tolist()

	# fill missing values with mean value
	for c in traintest.select_dtypes(exclude=['object']).columns:
		if traintest[c].isnull().sum() > 0 or (~np.isfinite(traintest[c])).sum() > 0:
			print('Feature: {} has missing values'.format(c))
			traintest.loc[(~np.isfinite(traintest[c])), c]  = traintest.loc[(np.isfinite(traintest[c])), c].mean()

	print('Feature names of one hot encoded features:\n', ohe_feature_names)
	joblib.dump(ohe_feature_names, os.path.join(basepath, 'data/interim/ohe_feature_names.pkl'))
	
	# drop the original column
	traintest.drop(features_to_ohe, axis=1, inplace=True)
	
	# combine ohe values
	traintest = pd.concat((traintest, one_hot_encoded_features), axis='columns')
	
	return traintest


def save_dataframes(split):
	train, test = get_data(split=split)

	ntrain = len(train)
	traintest = pd.concat((train, test))
	
	del train, test
	gc.collect()

	traintest = create_features(traintest, split)

	train = traintest.iloc[:ntrain]
	test  = traintest.iloc[ntrain:]

	if not split:
		FEATURES_TO_REMOVE = ['id', 'outcome', 'train', 'submission_id']
	else:
		FEATURES_TO_REMOVE = ['id', 'outcome', 'train']

	features = train.columns.drop(FEATURES_TO_REMOVE)

	X = scale(train.loc[:, features])
	y = train.loc[:, 'outcome'].astype(np.int16).values

	X_test = scale(test.loc[:, features])

	nn        = NearestNeighborsFeatures(n_neighbors=10, metric='minkowski', k_list=[3, 8, 16], n_jobs=4)
	train_knn = cross_val_predict(nn, X, y, cv=3, n_jobs=-1)

	nn.fit(X, y)
	test_knn  = nn.predict(X_test)

	knn_feature_names = ['knn_features_%s'%i for i in range(test_knn.shape[1])]

	train_knn         = pd.DataFrame(train_knn, columns=knn_feature_names, index=train.index.values)
	test_knn          = pd.DataFrame(test_knn, columns=knn_feature_names, index=test.index.values)

	print('knn train shape ', train_knn.shape)
	print('knn test shape ', test_knn.shape)

	train = pd.concat((train, train_knn), axis='columns')
	test  = pd.concat((test, test_knn), axis='columns')

	print('Save processed folds to disk ....')
	if split:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(test, os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(test, os.path.join(basepath, 'data/processed/%s_%s_test.feather'%(experiment_name, model_name)))

	return train, test

def load_dataframes(split):
	print('Loading datasets from disk ...')

	if split:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_test.feather'%(experiment_name, model_name)))


	return train, test


def cross_validate(X, y):
	params = {
			'objective': 'multiclass',
			'num_class': 3,
			'min_data_in_leaf': 20,
			'num_leaves': 16,
			'learning_rate': 0.01,
			'feature_fraction': 0.45,
			'feature_fraction_seed': SEED,
			'bagging_fraction': .9,
			'bagging_fraction_seed': SEED,
			'bagging_freq': 10,
			'metric': 'multi_logloss',
			'nthread': 4
		}

	num_boost_round       = 5000
	early_stopping_rounds = 100

	ltrain     = lgb.Dataset(X, y, feature_name=X.columns.tolist())
	cv_summary = lgb.cv(params, 
						ltrain, 
						num_boost_round,
						nfold=7,
						stratified=True,
						early_stopping_rounds=early_stopping_rounds,
						verbose_eval=20,
						seed=SEED
						)

	best_iteration = np.argmin(cv_summary['multi_logloss-mean'])
	params['num_boost_round'] = best_iteration

	print()

	return cv_summary['multi_logloss-mean'][best_iteration], cv_summary['multi_logloss-stdv'][best_iteration], params



def main(split):
	if split:

		if not os.path.exists(os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name))):
			print()
			print('Preparing dataframes ...')
			train, test = save_dataframes(split)

		else:
			print()
			print('Loading dataframes ...')
			train, test = load_dataframes(split)

		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)

		features = train.columns.drop(['outcome', 'id', 'train'])

		print('Feature in consideration ', list(features))
		print()
		print('Number of features ', len(features))
		print('*' * 100)

		X = train.loc[:, features]
		y = train.loc[:, 'outcome']

		X_test = test.loc[:, features]
		y_test = test.loc[:, 'outcome']

		# cross validate and find best parameters
		cv_mean, cv_std, params = cross_validate(X, y)
		
		print()
		print('Mean cv: %.5f and std: %.5f'%(cv_mean, cv_std))

		num_boost_round = params['num_boost_round']
		del params['num_boost_round']

		print()
		print('Best iteration: ', num_boost_round)

		ltrain = lgb.Dataset(X, y, feature_name=X.columns.tolist())
		
		print('*' * 150)
		print('Performance on holdout set')

		model      = lgb.train(params, ltrain, num_boost_round)
		hold_preds = model.predict(X_test)

		# save feature importance
		feature_imp      = pd.DataFrame({'feat': X.columns.tolist(),
									'imp' : model.feature_importance()
									}).sort_values(by='imp', ascending=False)
		print('\n\n')
		print('Feature Importance: ', feature_imp)
		print('Multi-class log loss ', log_loss(y_test, hold_preds))

		print()
		print('*' * 150)

		print('Saving model params and preds to disk ...')
		params['num_boost_round'] = num_boost_round
		joblib.dump(params, os.path.join(basepath, 'models/%s_%s_params.pkl'%(experiment_name, model_name)))
		joblib.dump(hold_preds, os.path.join(basepath, 'oof/%s_%s_preds.pkl'%(experiment_name, model_name)))

	else:
		"""
		Full Training
		"""
		if not os.path.exists(os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name))):
			train, test = save_dataframes(split)
		else:
			train, test = load_dataframes(split)

		print()
		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)
		
		features = train.columns.drop(['outcome', 'id', 'train', 'submission_id'])

		print('Feature in consideration ', list(features))

		X = train.loc[:, features]
		y = train.loc[:, 'outcome']

		# before creating X_test we would have to reindex using sample submission
		sub      = load_sub()
		test_sub = sub.merge(test, on='submission_id', how='left')
		X_test   = test_sub.loc[:, features]

		ltrain = lgb.Dataset(X, y)
		
		print()
		print('*' * 150)
		print('Loading parameters from the disk')
		params = joblib.load(os.path.join(basepath, 'models/%s_%s_params.pkl'%(experiment_name, model_name)))

		num_boost_round = int(params['num_boost_round'] * 1.1)
		del params['num_boost_round']

		params['learning_rate'] /=  1.1

		print()
		print('*' * 150)
		print('Parameters are\n', params)

		model = lgb.train(params, ltrain, num_boost_round)
		final_preds = model.predict(X_test)

		sub.loc[:, ['UE', 'FE', 'W']] = final_preds

		filename = create_filename(model_name, cv_score='0.32464+0.02571')
		sub.to_csv(os.path.join(basepath, 'submissions/%s_%s_%s.csv'%(experiment_name, model_name, filename)), index=False)

		print()
		print('*' * 150)
		params['num_boost_round'] = num_boost_round
		print('Saving model parameters to disk')
		joblib.dump(params, os.path.join(basepath, 'models/%s_%s_params_full.pkl'%(experiment_name, model_name)))


if __name__ == '__main__':
	print()
	print('AIM of the experiment')
	print(AIM)
	print('=' * 80)
	print()
	
	main(split=True)