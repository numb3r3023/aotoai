import pandas as pd
import numpy as np
import gc
import feather

from utils import *

from sklearn.externals import joblib
from sklearn.externals.joblib import Parallel, delayed
from sklearn.metrics import log_loss

from sklearn.preprocessing import OneHotEncoder, scale
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.linear_model import LogisticRegression

np.random.seed(SEED)

class FeatureTransformationEnsemble(BaseEstimator, ClassifierMixin):
	def __init__(self, model, n_jobs=1):
		self.model = model
		self.ohe   = OneHotEncoder()
		self.lm    = LogisticRegression()
		
	def fit(self, X, y=None):
		self.model.fit(X, y)
		self.ohe.fit(self.model.apply(X)[:, :, 0])
		self.lm.fit(self.ohe.transform(self.model.apply(X)[:, :, 0]), y)


	def predict(self, X):
		return self.lm.predict_proba(self.ohe.transform(self.model.apply(X)[:, :, 0]))