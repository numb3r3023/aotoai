AIM = """
Feature Learner that finds the best set of features to train on.
"""

import pandas as pd
import numpy as np
import gc
import feather

from utils import *

from sklearn.externals import joblib
from sklearn.metrics import log_loss
from sklearn.ensemble import GradientBoostingClassifier

import lightgbm as lgb

import warnings
warnings.filterwarnings('ignore')


experiment_name = 'EXPERIMENTFL'
model_name      = 'LGB%s'%(SEED)

np.random.seed(SEED)


def load_dataframes():
	print('Loading datasets from disk ...')

	train = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/EXPERIMENT40_%s_train.feather'%(model_name)))
	test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/EXPERIMENT40_%s_eval.feather'%(model_name)))
	
	return train, test


def choose_features(X, y):
	n_elements = X.shape[0]
	feats      = np.array(X.columns)

	n_feats    = X.shape[1]
	weights    = np.array(np.zeros(n_feats))
	weights   += 1 / n_feats

	n_features = 50

	best_feats = []
	best_score = np.inf
	epochs     = 30

	wt_g        = .2
	w_threshold = .2

	skf = StratifiedKFold(n_splits=3, shuffle=True, random_state=SEED)
	scores = []
	top_feats = np.array([])

	params = joblib.load(os.path.join(basepath, 'models/EXPERIMENT40_LGB1982_params.pkl'))

	del params['feature_fraction'], params['bagging_fraction']
	del params['feature_fraction_seed'], params['bagging_fraction_seed']
	del params['bagging_freq'], params['nthread']

	params['n_estimators'] = params['num_boost_round']
	params['silent']  = True
	params['verbose'] = 0
	params['n_jobs']  = -1

	del params['num_boost_round']

	print('Parameters are: ', params)
	print()

	for i, ind in enumerate(range(epochs)):
		print('Epoch={}'.format(i))
		sample_feat_ids = np.random.choice(n_feats, size=n_features, replace=False, p=weights)
		sample_feats    = np.append(top_feats, feats[sample_feat_ids])

		tst_P = np.zeros(shape=(n_elements, 3))


		for trn, tst in skf.split(X, y):
			trn_X, tst_X = X.iloc[trn].loc[:, sample_feats], X.iloc[tst].loc[:, sample_feats]
			trn_y, tst_y = y.iloc[trn], y.iloc[tst]



			model = lgb.LGBMClassifier(**params)
			model.fit(trn_X, trn_y)

			tst_P[tst] = model.predict_proba(tst_X) 


		tst_ll = log_loss(y, tst_P)


		if ind > 7:
			scores.append(tst_ll)
			ma_ll = np.mean(scores[-30:])

			if ind % 5 == 0:
				print(ind, ma_ll, tst_ll)

			if tst_ll > ma_ll:
				weights[sample_feat_ids] *= (1 + wt_g)
				sum_w = np.sum(weights)
				weights /= sum_w

				if tst_ll < best_score:
					best_score = tst_ll
					best_feats = sample_feats
			else:
				weights[sample_feat_ids] *= (1 - wt_g)
				sum_w = np.sum(weights)
				weights /= sum_w 

			mx_w = np.sum(weights)

			if mx_w > w_threshold:
				feat_imps = pd.Series(index = feats, data = weights)
				new_feats = feat_imps[feat_imps > w_threshold].index.values
				top_weights = feat_imps[feat_imps > w_threshold].values
				top_feats = np.append(top_feats, new_feats)
				feats = list(feats)
				weights = list(weights)

				for f,w in zip(new_feats,top_weights):
					print(f, w)
					feats.remove(f)
					weights.remove(w)

				n_feats = len(weights)
				feats = np.array(feats)
				weights = np.array(weights)
				sum_w = np.sum(weights)
				weights /= sum_w

			if ind % 5 == 0: 
				print(mx_w)

		else:
			scores.append(tst_ll)
			print(ind, tst_ll)

		print('*' * 100)
		print()

	print()
	print('Best score ', best_score)
	print('Top features ', top_feats)
	print('Probability = {}'.format(weights))


def main():
	train, test = load_dataframes()

	features = train.columns.drop(['outcome', 'id', 'train'])

	X = train.loc[:, features]
	y = train.loc[:, 'outcome']

	print('Features\n', features)
	# choose_features(X, y)

	

if __name__ == '__main__':
	print()
	print('AIM of the experiment')
	print(AIM)
	print('=' * 80)
	print()
	
	main()