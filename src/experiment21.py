AIM = """
Neural Network model on all features plus some following curated features.

1. Ratio of change in speed of current shot to penultimate shot.
2. Ratio of current depth to previous depth.
3. Ratio of current lateral distance from sideline to lateral distance of penultimate shot combined with
   distance travelled by player making the final shot.
4. Ratio of depth of player's position from net from penultimate shot to point ending shot.
5. Difference of depth of player's position from net from penultimate shot to point ending shot.
6. Ratio of player's depth.
7. Speed of the penultimate shot. ( Distance travelled by player receiving the shot ) / ( Time for 
   penultimate shot to be hit and pass the net)
8. Relationship between ball bounce distance from net and player's distance from net.
9. Relationship between ball bounce distance from sideline ( lateral ) and euclidean distance covered by player
   across last two shots.
10. Relationship between change in ball speed and distance covered by player across last two shots of the point.
11. Acceleration of ball speeds.

"""

import pandas as pd
import numpy as np
import gc
import feather

from utils import *

from sklearn.externals import joblib
from sklearn.metrics import log_loss
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import scale

import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.wrappers.scikit_learn import KerasClassifier

experiment_name = 'EXPERIMENT21'
model_name      = 'NN%s'%(SEED)

np.random.seed(SEED)

def create_features(traintest, split):

	if not split:
		# create a feature for helping to merge with submission file
		traintest.loc[:, 'submission_id'] = traintest.loc[:, 'id'].astype(np.str) + '_' + traintest.loc[:, 'gender']
	

	# flag if net clearance is negative or not
	traintest.loc[:, 'below_net'] = (traintest['net.clearance'] < 0).astype(np.uint8)
	
	# interaction between outside baseline and outside sideline net clearance greater than zero or not.
	# since this is a categorical variable we would have to one-hot encode this.
	traintest.loc[:, 'out_of_bounds_or_below_net'] = (traintest['outside.sideline']).astype(np.str) + '_' +\
													 (traintest['outside.baseline']).astype(np.str) + '_' +\
													 (traintest['net.clearance'] < 0). astype(np.str)
	

	# feature to capture ratio of change in speed from previous shot
	# to this shot.
	traintest.loc[:, 'speed_ratio'] = (traintest['speed'] / traintest['previous.speed'])

	# feature to capture ratio of current depth from baseline to previous shot
	traintest.loc[:, 'depth_ratio'] = (traintest['depth'] / traintest['previous.depth'])

	# feature to capture good angeled shot
	traintest.loc[:, 'angeled_shot'] = (traintest['distance.from.sideline'] / traintest['previous.distance.from.sideline']) * traintest['player.distance.travelled']

	# feature to capture change in player's depth from penultimate shot to point-ending shot
	traintest.loc[:, 'player_depth_ratio'] = (traintest['player.impact.depth'] / traintest['player.depth'])
	traintest.loc[:, 'player_depth_diff']  = (traintest['player.impact.depth'] - traintest['player.depth'])

	# feature to capture player's depths
	traintest.loc[:, 'player_depth_comp'] = (traintest['player.impact.depth'] / traintest['opponent.depth'])

	# feature to capture speed of penultimate shot's speed
	traintest.loc[:, 'prev_shot_speed']   = (traintest['player.distance.travelled'] / traintest['previous.time.to.net'])

	# feature to capture relationship between ball bounce distance from net and player's distance from net.
	traintest.loc[:, 'ball_player_depth_relation'] = (traintest['depth'] * traintest['player.impact.depth'])

	# feature to compare ball distance from sideline and distance travelled by player across last two shots
	# if lateral distance of ball from sideline is low and player distance is huge then kudos to opponent
	# for making the player returning the shot off-balance.

	traintest.loc[:, 'ball_player_dist_sideline'] = (traintest['previous.distance.from.sideline'] - traintest['distance.from.sideline']) / (traintest['player.distance.travelled'])

	# feature to capture difference between ball speeds and distance travelled by player across last two
	# shots of the point.

	traintest.loc[:, 'ball_speeds_player_dist'] = (traintest['speed'] - traintest['previous.speed']) / (traintest['player.distance.travelled'])

	# speed_diff = traintest['speed'] - traintest['previous.speed']
	# traintest.loc[:, 'drop_shot_char'] = speed_diff * traintest['previous.time.to.net']
	# traintest.loc[:, 'time_to_approach'] = (traintest['speed'] - traintest['previous.speed']) / traintest['player.impact.depth']
	# traintest.loc[:, 'distance_covered'] = traintest['speed'] * traintest['previous.time.to.net']
	traintest.loc[:, 'deceleration_rate']  = (traintest['speed'] - traintest['previous.speed']) / traintest['previous.time.to.net']
	
	# traintest.loc[:, 'drop_shot_location'] = (traintest['speed'] - traintest['previous.speed']) / traintest['distance.from.sideline']
	# traintest.loc[:, 'opponent_position_on_drop_shot'] = (traintest['speed'] - traintest['previous.speed']) / traintest['opponent.depth']
	traintest.loc[:, 'player_pos_center'] = (traintest['player.distance.from.center'] - traintest['player.impact.distance.from.center'])

	# One hot encode following categorical variables
	#
	# 1. Serve
	# 2. hitpoint
	# 3. outside.sideline
	# 4. outside.baseline
	# 5. same.side
	# 6. previous.hitpoint
	# 7. server.is.impact.player
	# 8. gender

	features_to_ohe = ['serve', 
					   'hitpoint',
					   'outside.sideline',
					   'outside.baseline',
					   'same.side',
					   'previous.hitpoint',
					   'server.is.impact.player',
					   'gender',
					   'out_of_bounds_or_below_net'
					  ]

	one_hot_encoded_features = one_hot_encode(traintest.loc[:, features_to_ohe].astype(np.str), features_to_ohe)
	ohe_feature_names        = one_hot_encoded_features.columns.tolist()


	# fill missing values with mean value
	for c in traintest.select_dtypes(exclude=['object']).columns:
		if traintest[c].isnull().sum() > 0 or (~np.isfinite(traintest[c])).sum() > 0:
			traintest.loc[(~np.isfinite(traintest[c])), c]  = traintest.loc[(np.isfinite(traintest[c])), c].mean()

	print('Feature names of one hot encoded features:\n', ohe_feature_names)
	joblib.dump(ohe_feature_names, os.path.join(basepath, 'data/interim/ohe_feature_names.pkl'))
	
	# drop the original column
	traintest.drop(features_to_ohe, axis=1, inplace=True)
	
	# combine ohe values
	traintest = pd.concat((traintest, one_hot_encoded_features), axis='columns')
	
	return traintest


def save_dataframes(split):
	train, test = get_data(split=split)

	ntrain    = len(train)
	traintest = pd.concat((train, test))
	
	del train, test
	gc.collect()

	traintest = create_features(traintest, split)

	train = traintest.iloc[:ntrain]
	test  = traintest.iloc[ntrain:]

	print('Save processed folds to disk ....')
	if split:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(test, os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(test, os.path.join(basepath, 'data/processed/%s_%s_test.feather'%(experiment_name, model_name)))

	return train, test

def load_dataframes(split):
	print('Loading datasets from disk ...')

	if split:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_test.feather'%(experiment_name, model_name)))


	return train, test

def model_configuration():
	# create model
	model = Sequential(name='mlp')

	model.add(Dense(units=50, activation='relu', input_dim=49))
	model.add(Dropout(rate=.1))
	model.add(Dense(units=20, activation='relu'))
	model.add(Dropout(rate=.1))
	model.add(Dense(units=3, activation='softmax'))

	model.compile("adam", "categorical_crossentropy", metrics=['accuracy'])

	return model

def cross_validate(X, y, labels):

	skf     = StratifiedKFold(n_splits=7, shuffle=False, random_state=SEED)
	fold_ll = []
	estimator = KerasClassifier(build_fn=model_configuration, epochs=10, verbose=0)

	for fold_index, (itr, ite) in enumerate(skf.split(X, labels)):

		print('Fold: ', fold_index)
		estimator.fit(X[itr], y[itr])
		fold_preds = estimator.predict_proba(X[ite])
		logloss = log_loss(labels.values[ite], fold_preds)

		print('Fold log loss ', logloss)
		fold_ll.append(log_loss(labels.values[ite], fold_preds))
	
	return np.mean(fold_ll), np.std(fold_ll)

def main(split):
	if split:

		if not os.path.exists(os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name))):
			train, test = save_dataframes(split)

		else:
			train, test = load_dataframes(split)

		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)

		features = train.columns.drop(['outcome', 'id', 'train'])

		print('Feature in consideration ', list(features))
		print()
		print('Number of features ', len(features))
		print('*' * 100)

		X = scale(train.loc[:, features])
		y = pd.get_dummies(train.loc[:, 'outcome']).values

		X_test = scale(test.loc[:, features])
		y_test = pd.get_dummies(test.loc[:, 'outcome']).values

		# cross validate and find best parameters
		cv_mean, cv_std = cross_validate(X, y, train.loc[:, 'outcome'])
		
		print()
		print('Mean cv: %.5f and std: %.5f'%(cv_mean, cv_std))

		print()
		print('*' * 150)
		print('Performance on holdout set')

		model = KerasClassifier(build_fn=model_configuration, epochs=10, verbose=0)
		model.fit(X, y)
		hold_preds = model.predict_proba(X_test)

		print('Multi-class log loss ', log_loss(test.loc[:, 'outcome'].values, hold_preds))

		print()
		print('*' * 150)

		print('Saving preds to disk ...')
		print('saving target fold to disk ...')
		joblib.dump(test.loc[:, 'outcome'].values, os.path.join(basepath, 'oof/preds.pkl'))
		joblib.dump(hold_preds, os.path.join(basepath, 'oof/%s_%s_preds.pkl'%(experiment_name, model_name)))

	else:
		"""
		Full Training
		"""
		if not os.path.exists(os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name))):
			train, test = save_dataframes(split)

		else:
			train, test = load_dataframes(split)

		print()
		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)
		
		features = train.columns.drop(['outcome', 'id', 'train', 'submission_id'])

		print('Feature in consideration ', list(features))

		X = scale(train.loc[:, features])
		y = pd.get_dummies(train.loc[:, 'outcome']).values

		# before creating X_test we would have to reindex using sample submission
		sub      = load_sub()
		test_sub = sub.merge(test, on='submission_id', how='left')
		X_test   = scale(test_sub.loc[:, features])

		model = KerasClassifier(build_fn=model_configuration, epochs=10, verbose=0)
		model.fit(X, y)
		final_preds = model.predict_proba(X_test)

		sub.loc[:, ['UE', 'FE', 'W']] = final_preds

		filename = create_filename(model_name, cv_score='0.36027+0.02157')
		sub.to_csv(os.path.join(basepath, 'submissions/%s_%s_%s.csv'%(experiment_name, model_name, filename)), index=False)

		print()
		print('*' * 150)
		

if __name__ == '__main__':
	print()
	print('AIM of the experiment')
	print(AIM)
	print('=' * 80)
	print()
	
	main(split=False)