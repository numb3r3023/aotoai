AIM = """
LightGBM model on all features plus some following curated features.

1. Ratio of change in speed of current shot to penultimate shot.
2. Ratio of current depth to previous depth.
3. Ratio of current lateral distance from sideline to lateral distance of penultimate shot combined with
   distance travelled by player making the final shot.
4. Ratio of depth of player's position from net from penultimate shot to point ending shot.
5. Difference of depth of player's position from net from penultimate shot to point ending shot.
6. Ratio of player's depth.
7. Speed of the penultimate shot. ( Distance travelled by player receiving the shot ) / ( Time for 
   penultimate shot to be hit and pass the net)
8. Relationship between ball bounce distance from net and player's distance from net.

"""

import pandas as pd
import numpy as np
import gc
import feather

from utils import *

from sklearn.externals import joblib
from sklearn.metrics import log_loss

import lightgbm as lgb


experiment_name = 'EXPERIMENT10'
model_name      = 'LGB%s'%(SEED)

np.random.seed(SEED)

def create_features(traintest, split):

	if not split:
		# create a feature for helping to merge with submission file
		traintest.loc[:, 'submission_id'] = traintest.loc[:, 'id'].astype(np.str) + '_' + traintest.loc[:, 'gender']
	

	# flag if net clearance is negative or not
	traintest.loc[:, 'below_net'] = (traintest['net.clearance'] < 0).astype(np.uint8)
	
	# interaction between outside baseline and outside sideline net clearance greater than zero or not.
	# since this is a categorical variable we would have to one-hot encode this.
	traintest.loc[:, 'out_of_bounds_or_below_net'] = (traintest['outside.sideline']).astype(np.str) + '_' +\
													 (traintest['outside.baseline']).astype(np.str) + '_' +\
													 (traintest['net.clearance'] < 0). astype(np.str)
	

	# feature to capture ratio of change in speed from previous shot
	# to this shot.
	traintest.loc[:, 'speed_ratio'] = (traintest['speed'] / traintest['previous.speed'])

	# feature to capture ratio of current depth from baseline to previous shot
	traintest.loc[:, 'depth_ratio'] = (traintest['depth'] / traintest['previous.depth'])

	# feature to capture good angeled shot
	traintest.loc[:, 'angeled_shot'] = (traintest['distance.from.sideline'] / traintest['previous.distance.from.sideline']) * traintest['player.distance.travelled']

	# feature to capture change in player's depth from penultimate shot to point-ending shot
	traintest.loc[:, 'player_depth_ratio'] = (traintest['player.impact.depth'] / traintest['player.depth'])
	traintest.loc[:, 'player_depth_diff']  = (traintest['player.impact.depth'] - traintest['player.depth'])

	# feature to capture player's depths
	traintest.loc[:, 'player_depth_comp'] = (traintest['player.impact.depth'] / traintest['opponent.depth'])

	# feature to capture speed of penultimate shot's speed
	traintest.loc[:, 'prev_shot_speed']   = (traintest['player.distance.travelled'] / traintest['previous.time.to.net'])

	# feature to capture relationship between ball bounce distance from net and player's distance from net.
	traintest.loc[:, 'ball_player_depth_relation'] = (traintest['depth'] * traintest['player.impact.depth'])

	# One hot encode following categorical variables
	#
	# 1. Serve
	# 2. hitpoint
	# 3. outside.sideline
	# 4. outside.baseline
	# 5. same.side
	# 6. previous.hitpoint
	# 7. server.is.impact.player
	# 8. gender

	features_to_ohe = ['serve', 
					   'hitpoint',
					   'outside.sideline',
					   'outside.baseline',
					   'same.side',
					   'previous.hitpoint',
					   'server.is.impact.player',
					   'gender',
					   'out_of_bounds_or_below_net'
					  ]

	one_hot_encoded_features = one_hot_encode(traintest.loc[:, features_to_ohe].astype(np.str), features_to_ohe)
	ohe_feature_names        = one_hot_encoded_features.columns.tolist()

	print('Feature names of one hot encoded features:\n', ohe_feature_names)
	joblib.dump(ohe_feature_names, os.path.join(basepath, 'data/interim/ohe_feature_names.pkl'))
	
	# drop the original column
	traintest.drop(features_to_ohe, axis=1, inplace=True)
	
	# combine ohe values
	traintest = pd.concat((traintest, one_hot_encoded_features), axis='columns')
	
	return traintest


def save_dataframes(split):
	train, test = get_data(split=split)

	ntrain = len(train)
	traintest = pd.concat((train, test))
	
	del train, test
	gc.collect()

	traintest = create_features(traintest, split)

	train = traintest.iloc[:ntrain]
	test  = traintest.iloc[ntrain:]

	print('Save processed folds to disk ....')
	if split:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/%s_%s_test_.feather'%(experiment_name, model_name)))

	return train, test

def load_dataframes(split):
	print('Loading datasets from disk ...')

	if split:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_test.feather'%(experiment_name, model_name)))


	return train, test


def cross_validate(X, y):
	params = {
			'objective': 'multiclass',
			'num_class': 3,
			'min_data_in_leaf': 20,
			'learning_rate': 0.01,
			'feature_fraction': 0.8,
			'feature_fraction_seed': SEED,
			'metric': 'multi_logloss',
			'nthread': 4
		}

	num_boost_round       = 5000
	early_stopping_rounds = 100

	ltrain     = lgb.Dataset(X, y, feature_name=X.columns.tolist())
	cv_summary = lgb.cv(params, 
						ltrain, 
						num_boost_round,
						nfold=7,
						stratified=True,
						early_stopping_rounds=early_stopping_rounds,
						verbose_eval=20,
						seed=SEED
						)

	best_iteration = np.argmin(cv_summary['multi_logloss-mean'])
	params['num_boost_round'] = best_iteration

	print()

	return cv_summary['multi_logloss-mean'][best_iteration], cv_summary['multi_logloss-stdv'][best_iteration], params



def main(split):
	if split:

		if not os.path.exists(os.path.join(basepath, 'data/processed/folds/%s_%s_train_fold.feather'%(experiment_name, model_name))):
			train, test = save_dataframes(split)

		else:
			train, test = load_dataframes()

		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)

		features = train.columns.drop(['outcome', 'id'])

		print('Feature in consideration ', list(features))

		X = train.loc[:, features]
		y = train.loc[:, 'outcome']

		X_test = test.loc[:, features]
		y_test = test.loc[:, 'outcome']

		# cross validate and find best parameters
		cv_mean, cv_std, params = cross_validate(X, y)
		
		print()
		print('Mean cv: %.5f and std: %.5f'%(cv_mean, cv_std))

		num_boost_round = params['num_boost_round']
		del params['num_boost_round']

		print()
		print('Best iteration: ', num_boost_round)

		ltrain = lgb.Dataset(X, y, feature_name=X.columns.tolist())
		
		print('*' * 150)
		print('Performance on holdout set')

		model      = lgb.train(params, ltrain, num_boost_round)
		hold_preds = model.predict(X_test)

		# save feature importance
		feature_imp      = pd.DataFrame({'feat': X.columns.tolist(),
									'imp' : model.feature_importance()
									}).sort_values(by='imp', ascending=False)
		print('\n\n')
		print('Feature Importance: ', feature_imp)
		print('Multi-class log loss ', log_loss(y_test, hold_preds))

		print()
		print('*' * 150)

		print('Saving model params and preds to disk ...')
		params['num_boost_round'] = num_boost_round
		joblib.dump(params, os.path.join(basepath, 'models/%s_%s_params.pkl'%(experiment_name, model_name)))
		joblib.dump(hold_preds, os.path.join(basepath, 'oof/%s_%s_preds.pkl'%(experiment_name, model_name)))

	else:
		"""
		Full Training
		"""
		if not os.path.exists(os.path.join(basepath, 'data/processed/folds/%s_%s_train_fold.feather'%(experiment_name, model_name))):
			train, test = save_dataframes(split)

		else:
			train, test = load_dataframes(split)

		print()
		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)

		features = train.columns.drop(['outcome', 'id', 'submission_id'])

		print('Feature in consideration ', list(features))

		X = train.loc[:, features]
		y = train.loc[:, 'outcome']

		# before creating X_test we would have to reindex using sample submission
		sub      = load_sub()
		test_sub = sub.merge(test, on='submission_id', how='left')
		X_test   = test_sub.loc[:, features]

		ltrain = lgb.Dataset(X, y)
		
		print()
		print('*' * 150)
		print('Loading parameters from the disk')
		params = joblib.load(os.path.join(basepath, 'models/%s_%s_params.pkl'%(experiment_name, model_name)))

		num_boost_round = int(params['num_boost_round'] * 1.1)
		del params['num_boost_round']

		params['learning_rate'] /=  1.1

		print()
		print('*' * 150)
		print('Parameters are\n', params)

		model = lgb.train(params, ltrain, num_boost_round)
		final_preds = model.predict(X_test)

		sub.loc[:, ['UE', 'FE', 'W']] = final_preds

		filename = create_filename(model_name, cv_score='0.31461+0.02579')
		sub.to_csv(os.path.join(basepath, 'submissions/%s.csv'%(filename)), index=False)

		print()
		print('*' * 150)
		params['num_boost_round'] = num_boost_round
		print('Saving model parameters to disk')
		joblib.dump(params, os.path.join(basepath, 'models/%s_%s_params_full.pkl'%(experiment_name, model_name)))


if __name__ == '__main__':
	print()
	print('AIM of the experiment')
	print(AIM)
	print('=' * 80)
	print()
	
	main(split=False)