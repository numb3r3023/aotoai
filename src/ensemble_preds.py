AIM = """
ENSEMBLE EXPERIMENT 33 and EXPERIMENT 35 preds
"""

import pandas as pd
import numpy as np
import gc
import feather

from utils import *
from sklearn.externals import joblib

experiment_name = 'ENSEMBLE_33_35'
model_name      = 'LGB_33_35%s'%(SEED)

np.random.seed(SEED)

def main():
	exp_33 = pd.read_csv(os.path.join(basepath, 'submissions/EXPERIMENT33_LGB1982_20180112_2319_LGB1982_0_31274+0_02381_0.18096.csv'))
	exp_35 = pd.read_csv(os.path.join(basepath, 'submissions/EXPERIMENT35_LGB1982_20180114_1127_LGB1982_0_31558+0_02436.csv'))

	ensemble_df    = exp_33.copy()
	ensemble_preds = 0.8 * exp_33.iloc[:, -3:] + 0.2 * exp_35.iloc[:, -3:]

	ensemble_df.iloc[:, -3:] = ensemble_preds
	
	filename = create_filename(model_name, cv_score='0.0')
	ensemble_df.to_csv(os.path.join(basepath, 'submissions/%s_%s_%s.csv'%(experiment_name, model_name, filename)), index=False)

if __name__ == '__main__':
	main()