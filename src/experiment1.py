AIM = """
Train an XGBoost Model on all the features and save feature importance.
"""

import pandas as pd
import numpy as np
import gc
import feather

from utils import *

from sklearn.externals import joblib
from sklearn.metrics import log_loss

import xgboost as xgb


experiment_name = 'EXPERIMENT1'
model_name      = 'XGB%s'%(SEED)

np.random.seed(SEED)

def create_features(traintest, split):

	if not split:
		# create a feature for helping to merge with submission file
		traintest.loc[:, 'submission_id'] = traintest.loc[:, 'id'].astype(np.str) + '_' + traintest.loc[:, 'gender']
	

	# flag if net clearance is negative or not
	traintest.loc[:, 'below_net'] = (traintest['net.clearance'] < 0).astype(np.uint8)
	
	# interaction between outside baseline and outside sideline net clearance greater than zero or not.
	# since this is a categorical variable we would have to one-hot encode this.
	traintest.loc[:, 'out_of_bounds_or_below_net'] = (traintest['outside.sideline']).astype(np.str) + '_' +\
													 (traintest['outside.baseline']).astype(np.str) + '_' +\
													 (traintest['net.clearance'] < 0). astype(np.str)
	
	# One hot encode following categorical variables
	# 1. Serve
	# 2. hitpoint
	# 3. outside.sideline
	# 4. outside.baseline
	# 5. same.side
	# 6. previous.hitpoint
	# 7. server.is.impact.player
	# 8. gender

	features_to_ohe = ['serve', 
					   'hitpoint',
					   'outside.sideline',
					   'outside.baseline',
					   'same.side',
					   'previous.hitpoint',
					   'server.is.impact.player',
					   'gender',
					   'out_of_bounds_or_below_net'
					  ]

	one_hot_encoded_features = one_hot_encode(traintest.loc[:, features_to_ohe].astype(np.str), features_to_ohe)
	ohe_feature_names = one_hot_encoded_features.columns.tolist()

	print('Feature names of one hot encoded features:\n', ohe_feature_names)
	joblib.dump(ohe_feature_names, os.path.join(basepath, 'data/interim/ohe_feature_names.pkl'))
	
	# drop the original column
	traintest.drop(features_to_ohe, axis=1, inplace=True)
	
	# combine ohe values
	traintest = pd.concat((traintest, one_hot_encoded_features), axis='columns')
	
	return traintest


def save_dataframes(split):
	train, test = get_data(split=split)

	ntrain = len(train)
	traintest = pd.concat((train, test))
	
	del train, test
	gc.collect()

	traintest = create_features(traintest, split)

	train = traintest.iloc[:ntrain]
	test  = traintest.iloc[ntrain:]

	print('Save processed folds to disk ....')
	if split:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		feather.write_dataframe(train, os.path.join(basepath, 'data/processed/%s_%s_test_.feather'%(experiment_name, model_name)))

	return train, test

def load_dataframes(split):
	print('Loading datasets from disk ...')

	if split:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/folds/%s_%s_eval.feather'%(experiment_name, model_name)))
	else:
		train = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_train.feather'%(experiment_name, model_name)))
		test  = feather.read_dataframe(os.path.join(basepath, 'data/processed/%s_%s_test.feather'%(experiment_name, model_name)))


	return train, test

def main(split):
	if split:

		if not os.path.exists(os.path.join(basepath, 'data/processed/folds/%s_%s_train_fold.feather'%(experiment_name, model_name))):
			train, test = save_dataframes(split)

		else:
			train, test = load_dataframes()

		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)

		features = train.columns.drop(['outcome', 'id'])

		print('Feature in consideration ', list(features))

		X = train.loc[:, features]
		y = train.loc[:, 'outcome']

		X_test = test.loc[:, features]
		y_test = test.loc[:, 'outcome']


		dtrain = xgb.DMatrix(X, y)
		deval  = xgb.DMatrix(X_test, y_test)

		num_boost_round = 100

		params = {
			'objective': 'multi:softprob',
			'max_depth': 4,
			'eval_metric': 'mlogloss',
			'num_class': 3,
			'seed': SEED,
			'verbose': 1,
			'nthread': 4,
			'silent': 1
		}

		model      = xgb.train(params, dtrain, num_boost_round)
		hold_preds = model.predict(deval)

		# save feature importance
		feature_imp_dict = model.get_score()
		feature_imp      = pd.DataFrame({'feature_names' : list(feature_imp_dict.keys()),
										 'importance': list(feature_imp_dict.values())
									}).sort_values(by='importance', ascending=False)

		print('\n\n')
		print('Feature Importance: ', feature_imp)
		print('Multi-class log loss ', log_loss(y_test, hold_preds))

	else:
		"""
		Full Training
		"""
		if not os.path.exists(os.path.join(basepath, 'data/processed/folds/%s_%s_train_fold.feather'%(experiment_name, model_name))):
			train, test = save_dataframes(split)

		else:
			train, test = load_dataframes(split)


		print('Shape of training data ', train.shape)
		print('Shape of test data ', test.shape)

		features = train.columns.drop(['outcome', 'id', 'submission_id'])

		print('Feature in consideration ', list(features))

		X = train.loc[:, features]
		y = train.loc[:, 'outcome']

		# before creating X_test we would have to reindex using sample submission
		sub      = load_sub()
		test_sub = sub.merge(test, on='submission_id', how='left')
		X_test   = test_sub.loc[:, features]

		dtrain = xgb.DMatrix(X, y)
		deval  = xgb.DMatrix(X_test)

		num_boost_round = 100

		params = {
			'objective': 'multi:softprob',
			'max_depth': 4,
			'eval_metric': 'mlogloss',
			'num_class': 3,
			'seed': SEED,
			'verbose': 1,
			'silent': 1
		}

		model = xgb.train(params, dtrain, num_boost_round)
		hold_preds = model.predict(deval)

		sub.loc[:, ['UE', 'FE', 'W']] = hold_preds

		filename = create_filename(model_name, cv_score=0.0)
		sub.to_csv(os.path.join(basepath, 'submissions/%s.csv'%(filename)), index=False)


if __name__ == '__main__':
	print('AIM of the experiment')
	print(AIM)
	print('=' * 80)
	print()
	
	main(split=True)